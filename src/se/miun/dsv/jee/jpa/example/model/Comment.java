package se.miun.dsv.jee.jpa.example.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

@Entity
@Table(name ="COMMENTS")
public class Comment implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long id;
	private String comment;
	LocalDateTime time;
	LocalDateTime lastUpdate;
	
	private Event event;
	private User user;
	
	public Comment(String comment, LocalDateTime time) {
		this.comment = comment;
		this.time = time;
	}
	
	public Comment() {
		
	}
	
	@Id
	@Column(name ="COMMENT_ID")
	@GeneratedValue
	public long getId() {
		return id;
	}
	
    @ManyToOne(cascade=CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinColumn(name = "EVENT_ID")
    public Event getEvent() {
        return event;
    }
    
    @ManyToOne(cascade=CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinColumn(name = "USER_ID")
    public User getUser() {
        return user;
    }
    
    public void setId(long id) {
    	this.id = id;
    }
    
    public void setEvent(Event event) {
    	this.event = event;
    }
    
    public void setUser(User user) {
    	this.user = user;
    }
    
    public void setComment(String comment) {
    	this.comment = comment;
    }
    
    public String getComment() {
    	return comment;
    }
    
	@PrePersist
	private void lastUpdatePrePersist() {
		lastUpdate = LocalDateTime.now();
	}
	
    @PreUpdate
    private void lastUpdatePreUpdate() {
    	lastUpdate = LocalDateTime.now();
    }
    
    public  void setTime(LocalDateTime time) {
    	this.time = time;
    }
    
    public LocalDateTime getTime() {
    	return time;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
    	this.lastUpdate = lastUpdate;
    }
    
    public LocalDateTime getLastUpdate() {
    	return lastUpdate;
    }
}

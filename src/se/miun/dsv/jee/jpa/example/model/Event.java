package se.miun.dsv.jee.jpa.example.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

@Entity
@Table(name = "EVENTS")
public class Event implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private long id;
	private String title;
	private String city;
	private LocalDateTime startTime;
	private LocalDateTime endTime;
	private LocalDateTime lastUpdate;
	private String content;
	
	private Set<Organizer> organizers = new HashSet<Organizer>();
	private Set<Comment> comments = new HashSet<Comment>();
	
	public Event(String title,String city,String content, LocalDateTime startTime, LocalDateTime endTime){
		this.title = title;
		this.city = city;
		this.startTime = startTime;
		this.endTime = endTime;
		this.content = content;
	}
	
	public Event() {
		
	}
	
	@Id
	@GeneratedValue
	@Column(name = "EVENT_ID")
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public LocalDateTime getStartTime() {
		return startTime;
	}
	
	public void setStartTime(LocalDateTime startTime){
		this.startTime = startTime;
	}
	
	public LocalDateTime getEndTime() {
		return endTime;
	}
	
	public void setEndTime(LocalDateTime endTime){
		this.endTime = endTime;
	}
	
	public LocalDateTime getLastUpdate() {
		return lastUpdate;
	}
	
	public void setLastUpdate(LocalDateTime lastUpdate){
		this.lastUpdate = lastUpdate;
	}
	
	@PrePersist
	private void lastUpdatePrePersist() {
		lastUpdate = LocalDateTime.now();
	}
	
    @PreUpdate
    private void lastUpdatePreUpdate() {
    	lastUpdate = LocalDateTime.now();
    }
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	public Set<Organizer> getOrganizers() {
		return this.organizers;
	}
	
	public void setOrganizers(Set<Organizer> organizers) {
		this.organizers = organizers;
	}
	
	public void addOrganizers(Organizer organizer) {
		this.organizers.add(organizer);
	}
	
    @OneToMany(mappedBy = "event", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public Set<Comment> getComments() {
        return comments;
    }
    
    public void setComments(Set<Comment> comments) {
    	this.comments = comments;
    }
	
    public void addComment(Comment comment) {
    	comments.add(comment);
    }
	
	

}

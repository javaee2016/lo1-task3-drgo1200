package se.miun.dsv.jee.jpa.example;

import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import se.miun.dsv.jee.jpa.example.model.User;


public class DAO {

	private static EntityManagerFactory entityManagerFactory = Persistence
			.createEntityManagerFactory("example");

	public void persistAll(Set<User> events) {
		EntityManager manager = entityManagerFactory.createEntityManager();

		EntityTransaction tx = manager.getTransaction();
		try {

			tx.begin();

			for (User event : events)
				manager.persist(event);

			tx.commit();

		} catch (Exception e) {
			tx.rollback();
			e.printStackTrace();
		} finally {
			manager.close();
		}
	}

	public void shutDown() {
		entityManagerFactory.close();
	}
}


package se.miun.dsv.jee.jpa.example;

import java.util.HashSet;
import java.util.Set;

import se.miun.dsv.jee.jpa.example.model.User;

public class DatabasePopulator {


	public static void main(String[] args) {
		Set<User> events = createEvents();
		DAO dao = new DAO();
		dao.persistAll(events);
		dao.shutDown();
	}

	private static Set<User> createEvents() {
		EventsParser ep = new EventsParser();
		ep.parse();
		Set<User> events = new HashSet<>();
		events = ep.getUsers();
		return events;
	}
		
}
